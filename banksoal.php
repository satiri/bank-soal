<?php

/*
Plugin Name: Bank SOAL
Plugin URI: https://satiri@bitbucket.org/satiri/bank-soal.git
Description: Manager Bank Soal Yang Memudahkan untuk menyimpan dan mengelola soal.
Author: Ahmad Satiri ( satiri.a@gmail.com )
Version: 1.0
Author URI: http://www.makkul.com/
*/

require_once(dirname(__FILE__) . '/classes/posttypes/soal.php');

Soal::registerPostType();
