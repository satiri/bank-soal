<?php
/**
 * Plugin Bank Soal
 * Ahmad Satiri 2013
 * Memudahkan menyimpan soal
 */
class Soal
{
    CONST POSTTYPE_NAME = "soal";

    /**
     * Only when activated
     */
    public static function install()
    {

    }

    public static function registerPostType()
    {
        $label = "bank_soal";

        $labels = array(
            'name' => __('Soal', $label),
            'singular_name' => __('Soal', $label),
            'add_new' => __('Add New', $label),
            'add_new_item' => __('Add New Soal', $label),
            'edit_item' => __('Edit Soal', $label),
            'new_item' => __('New Soal', $label),
            'view_item' => __('View Soal', $label),
            'search_items' => __('Search Soal', $label),
            'not_found' => __('No Soal Found', $label),
            'not_found_in_trash' => __('No Soal in Trash', $label),
            'parent_item_colon' => ''
        );

        $support = array('title', 'editor', 'thumbnail', 'excerpt',
            'category', 'slug', 'categories'
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'rewrite' =>
            array('slug' => Soal::POSTTYPE_NAME, 'with_front' => false),
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 5,
            'supports' => $support,
            'has_archive' => true
        );

        register_post_type(Soal::POSTTYPE_NAME, $args);
    }
}

?>
